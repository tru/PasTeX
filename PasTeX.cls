\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{PasTeX}[2018/10/25 v2.0]
\LoadClass[aspectratio=141]{beamer}
\RequirePackage[utf8]{inputenc}
\RequirePackage{lmodern}
\RequirePackage[absolute,overlay]{textpos}
\RequirePackage{setspace}

\definecolor{pasteur-blue}{RGB}{0,167,231}
\definecolor{pasteur-lightblue}{RGB}{176,205,237}
\definecolor{pasteur-orange}{RGB}{245,156,0}
\definecolor{pasteur-white}{RGB}{255,255,255}
\definecolor{pasteur-sepia}{RGB}{191,184,176}
\definecolor{pasteur-lightsepia}{RGB}{227,224,216}
\definecolor{pasteur-black}{RGB}{0,0,0}
\newcommand{\mlarge}{\@setfontsize{\mlarge}{12pt}{12pt}} 
\def\thankstitle#1{\def\@thankstitle{#1}}
\def\thanksmessage#1{\def\@thanksmessage{#1}}
\newcounter{toccpt}
\newcounter{seccpt}

% Header
\setbeamertemplate{headline}
{
    \begin{beamercolorbox}[wd=\paperwidth,ht=17mm]{headtxt}
        \vspace{3mm}
        \hspace{6mm}
        \huge{\insertsubsection}
    \end{beamercolorbox}
    \hspace{6mm}
    \rule{0.9\textwidth}{0.5pt}
}


% Footer
\setbeamertemplate{footline}
{
    \color{pasteur-black}
    \tiny\qquad\enskip\insertframenumber\enskip$\vert$\enskip\insertauthor\enskip$\vert$\enskip\inserttitle\enskip$\vert$\enskip\insertdate\enskip\enskip\enskip\enskip
    \rule{4.5cm}{0.1pt}\enskip\enskip\enskip\enskip
    \includegraphics[width=1.4cm]{img/logo_ip.png}
    \vspace*{3mm}
}


% List
\setbeamertemplate{itemize item}{\Large\color{pasteur-blue}$\bullet$}
\setbeamertemplate{itemize subitem}{\large\color{pasteur-blue}$\bullet$}
\setbeamertemplate{itemize subsubitem}{\normalsize\color{pasteur-blue}$\bullet$}


% General theme
\setbeamertemplate{navigation symbols}{}
\setbeamercolor{normal text}{fg=pasteur-black}
\setbeamertemplate{frametitle}{\color{pasteur-blue}\vspace{1mm}\bfseries\insertframetitle}


% First slide (title)
\renewcommand{\maketitle}
{
    {
        \usebackgroundtemplate{\begin{picture}(100,200)(0,100)\includegraphics[width=\paperwidth]{img/Part-0.jpg}\end{picture}}
        \begin{frame}[plain]
            \begin{textblock*}{5cm}(11.5cm,0.35cm)
                \includegraphics[width=2.5cm]{img/logo_ip.png}
            \end{textblock*}
            \hspace{-12mm}
            \vspace{-27mm}
            \colorbox{pasteur-white}
            {
                \begin{minipage}[t][4.1cm][c]{0,54\textwidth}
                    \begin{color}{pasteur-black}
                        \vspace{10mm}
                        \hspace{10mm}
                        \begin{minipage}[t][3.83cm][c]{0,9\textwidth}
                            \begin{spacing}{0.8}
                                \flushleft
                                \mlarge\textrm{\textbf{\inserttitle}}
                                \rule{4.8cm}{0.4pt}
                            \end{spacing}
                            \vspace{4mm}
                            \begin{spacing}{0.8}
                                \footnotesize\textbf{{\insertsubtitle}}
                            \end{spacing}
                            \vspace{6mm}
                            \color{pasteur-blue} \noindent {\small$\bullet$}~{\insertauthor}~~~~~~{\small$\bullet$}~{\insertdate}
                        \end{minipage}
                    \end{color}
                \end{minipage}
            }
        \end{frame}
    }
}


% Table Of Contents
\def\toc
{
    {
        \defbeamertemplate*{headline}{}
        {
            \begin{beamercolorbox}[wd=\paperwidth,ht=20mm]{headtxt}
                \vspace{3mm}
                \hspace{6mm}
                \huge{\english{Summary}\french{Sommaire}}
            \end{beamercolorbox}
            \hspace{6mm}
            \rule{0.9\textwidth}{0.5pt}
        }
        \setbeamercolor{headtxt}{bg=pasteur-white,fg=pasteur-black}
            
        \begin{frame}
            \begin{columns}[totalwidth=1\textwidth]
                \begin{column}[t]{.33\textwidth}
                    \setcounter{toccpt}{1}
                    \tableofcontents[hideallsubsections,sections={1}]
                    \vspace{10mm}
                    \setcounter{toccpt}{4}
                    \tableofcontents[hideallsubsections,sections={4}]
                \end{column}
                \begin{column}[t]{.33\textwidth}
                    \setcounter{toccpt}{2}
                    \tableofcontents[hideallsubsections,sections={2}]
                    \vspace{10mm}
                    \setcounter{toccpt}{5}
                    \tableofcontents[hideallsubsections,sections={5}]
                \end{column}
                \begin{column}[t]{.33\textwidth}
                    \setcounter{toccpt}{3}
                    \tableofcontents[hideallsubsections,sections={3}]
                    \vspace{10mm}
                    \setcounter{toccpt}{6}
                    \tableofcontents[hideallsubsections,sections={6}]
                \end{column}
            \end{columns}
        \end{frame}
    }
}

% section in TOC
\setbeamertemplate{section in toc}
{
    \centering
    \ifodd\thetoccpt
        \begin{color}{pasteur-lightblue}
            \Huge{0\inserttocsectionnumber}
        \end{color}
    \else
        \begin{color}{pasteur-blue}
            \Huge{0\inserttocsectionnumber}
        \end{color}
    \fi
    \\
	\begin{color}{pasteur-black}
        \rule{0.4cm}{1.5pt}\\
        \vspace{1mm}
		\normalsize{\inserttocsection \par}
	\end{color}
	\vspace{5mm}
}


% Sections
\AtBeginSection[]
{
    {
        \defbeamertemplate*{headline}{}
        
        \usebackgroundtemplate{\begin{picture}(100,200)(0,10)\includegraphics[width=\paperwidth]{img/Part-\theseccpt.jpg}\end{picture}}
        \stepcounter{seccpt}
        \begin{frame}
        \hspace{-12mm}
            \colorbox{pasteur-white}
            {
                \begin{minipage}[t][4.1cm][c]{0,54\textwidth}
                    \begin{color}{pasteur-black}
                        \vspace{10mm}
                        \hspace{10mm}
                        \begin{minipage}[t][3.83cm][c]{0,67\textwidth}
                            \begin{spacing}{0.8}
                                \flushleft
                                \mlarge\textrm{\textbf{\english{Part}\french{Partie} \insertsectionnumber}}
                                \rule{4.8cm}{0.4pt}
                            \end{spacing}
                            \vspace{4mm}
                            \begin{spacing}{0.8}
                                \footnotesize\textbf{{\insertsection}\vspace{8mm}}
                            \end{spacing}
                        \end{minipage}
                    \end{color}
                \end{minipage}
            }
        \end{frame}
    }
}


% Last slide
\def\makethanks
{
    {
        \defbeamertemplate*{headline}{}{
            \color{pasteur-blue}
            \bfseries\Large\centerline\@thankstitle
        }
        \defbeamertemplate*{footline}{}
        {
            \hspace{-22mm}
            \begin{minipage}{1.29\textwidth}
                \fontsize{5}{5.5}\selectfont
                {
                    \begin{color}{pasteur-blue}
                        \begin{columns}[b]
                            \begin{column}[c]{.11\textwidth}
                                \begin{column}[t]{.8\textwidth}
                                    \english{Public benefit foundation with official charitable status}\french{Fondation reconnue d'utilité publique habilitée à recevoir dons et legs}
                                \end{column}
                            \end{column}
                            \hfill
                            \begin{column}[c]{.3\textwidth}
                                \flushright
                                \english{\textbf{Institut Pasteur}\\25-28, rue du Docteur Roux\\75724 Paris Cedex 15 - France\\\phantom{1}}\french{\textbf{Institut Pasteur}\\25-28, rue du Docteur Roux\\75724 Paris Cedex 15\\\phantom{1}}
                            \end{column}
                        \end{columns}
                    \end{color}
                }
            \end{minipage}
        }

        \begin{frame}
            \begin{center}
                \@thanksmessage
            \end{center}  
        \end{frame}
    }
}