Beamer (LaTeX) template really closed to the official pptx Pasteur one.

##################
#### LANGUAGE ####
##################
You can set the language using the first line, either english or french:
`\usepackage[english]{languagechooser}`
or
`\usepackage[french]{languagechooser}`


#######################
#### PARTS and IMG ####
#######################
You can *NOT* have more than 6 parts, this is due to pptx Pasteur template.

You can change default images for each part.
The first one is `Part-0.jpg`
Then, each of the 6 parts have the corresponding `Part-X.jpg`

Images should be in `1760*1080px` and `100ppi`
Alternatively, you can have `880*540px`.
Always keep ratio of *1.63*.

You should put those images in `jpg` to save space and not end up with a pdf of several hundred of Mb.


################
#### SLIDES ####
################

Use `\maketitle` to have the first slide with title

Use `\toc` to have the summary slide

Use `\makethanks` to have the last slide with personalized message.
This message can be more complex than just raw text, i.e.:
`\thanksmessage{~\\\begin{center}\includegraphics<2>[width=0.32\textwidth]{img/question.png}\end{center}Any question?}`


PasTeX iz in da pl4ce!